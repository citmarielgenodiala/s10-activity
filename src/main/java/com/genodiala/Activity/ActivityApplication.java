package com.genodiala.Activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class ActivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityApplication.class, args);
	}

	// Get all users
	@GetMapping("/users")
	public String getAllUsers(){
		return "All users retrieved";
	}

	// Create User
	@PostMapping("/users")
	public String createUser(){
		return "New user created" ;
	}

	//Get a specific user
	@GetMapping("/users/{userId}")
	public String getUserByUserId(@PathVariable Long userId){
		return "The user with the ID " +userId+ " is being retrieved.";
	}

	// Delete a user && Refactor the delete user route to include a conditional statement to return a successful message if the request header’s authorization is not empty and an unauthorized access if it is.
	@DeleteMapping("/users/{userId}")
	public String deleteUser(@PathVariable Long userId, @RequestHeader(value = "Authorization", required = false) String authorizationHeader){
		if (authorizationHeader == null || authorizationHeader.isEmpty()) {
			return "Unauthorized access";
		}
		return "The user " + userId + " has been deleted";
	}

	// Update user
	@PutMapping("/users/{userId}")
	@ResponseBody
	public User updateUser(@PathVariable Long userId, @RequestBody User user){
		return user;
	}


}
